# Saleson Recruitment Task

## Installing

### Requirements

You will need `docker` and `docker-compose` packages.

### Running docker

Use `docker-compose up` in main directory.

### Installing symfony & packages

Use `docker exec -it rec-task-web composer install`

### Check application

Application should be online on http://localhost:8080
