<?php
namespace App\Form;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class TransferCreditForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('credits', NumberType::class)
            ->add('user', EntityType::class, [
                    'class' => User::class,
                    'query_builder' => function (EntityRepository $er) {
                       return $er->createQueryBuilder('u');
                     },
                ]
            )
            ->add('save', SubmitType::class)
        ;
    }
}