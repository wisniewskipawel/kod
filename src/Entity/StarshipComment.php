<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 */
class StarshipComment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ?Starship
     * @ORM\ManyToOne(targetEntity="App\Entity\Starship", inversedBy="comments")
     */
    private $starship;

    /**
     * @var ?string
     * @ORM\Column(type="string")
     */
    private $comment;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Starship|null
     */
    public function getStarship(): ?Starship
    {
        return $this->starship;
    }

    /**
     * @param Starship|null $starship
     */
    public function setStarship(?Starship $starship): void
    {
        $this->starship = $starship;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }



}