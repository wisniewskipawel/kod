<?php

namespace App\Command;

use App\Entity\Starship;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class DownloadStarshipsCommand extends Command
{
    protected static $defaultName = 'app:download-starship';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $count = json_decode(
            file_get_contents('https://swapi.dev/api/starships')
        )->count;

        for($i=1;$i<=$count;$i++) {

            try {
                $info = json_decode(file_get_contents('https://swapi.dev/api/starships/'.$i));

                $exists = $this->entityManager->getRepository(Starship::class)->find($i);

                if(!$exists) {
                    $model = new Starship();
                    $model->setId($i);
                }

                $model->setName($info->name);
                $model->setCost($info->cost_in_credits);

                $this->entityManager->persist($model);
            } catch(\Exception $e) {
                $output->writeln($e->getMessage());
            }

        }

        $this->entityManager->flush();
        $output->writeln('dane zapisane');
        return Command::SUCCESS;
    }

}