<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminCommand extends Command
{

    protected static $defaultName = 'app:create-admin';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordHasherInterface
     */
    private $userPasswordHasher;


    public function __construct(
        string $name = null,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $userPasswordHasher
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    protected function configure(): void
    {
        $this->addArgument('email', InputArgument::REQUIRED, 'The username of the user.');
        $this->addArgument('password', InputArgument::REQUIRED, 'The password of the user.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $model = new User();

        $model->setEmail($input->getArgument('email'));
        $model->setPassword(
            $this->userPasswordHasher->hashPassword(
                $model,
                $input->getArgument('password')
            )
        );
        $model->setRoles(['ROLE_ADMIN']);

        $this->entityManager->persist($model);
        $this->entityManager->flush();

        $output->writeln('An account '.$model->getEmail().' has been created');
        return Command::SUCCESS;
    }

}