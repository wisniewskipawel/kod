<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Starship;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BasketController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/basket")
     */
    public function index(): Response
    {
        if (!$this->getUser()) {
            throw new AccessDeniedException();
        }

        $baskets =
        $this->entityManager->createQueryBuilder()
        ->select('orders.id', 'starship.name', 'starship.cost')
        ->from(Order::class, 'orders')
        ->join(Starship::class, 'starship')
        ->where('starship.id = orders.starship')
        ->andWhere('orders.user = '.$this->getUser()->getId())
        ->getQuery()->getResult();

        return $this->render(
            'basket/index.html.twig',
            ['baskets' => $baskets]
        );
    }

    /**
     * @Route("/basket/trash/{id}")
     */
    public function trash(int $id)
   {
        if (!$this->getUser()) {
            throw new AccessDeniedException();
        }

        $user = $this->entityManager->getRepository(User::class)->find($this->getUser()->getId());

        $this->getUser()->setCredits($this->getUser()->getCredits() + $this->findPrice($id)[0]['cost']);        

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->entityManager->createQueryBuilder()
            ->delete()
            ->from(Order::class, 'orders')
            ->where('orders.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();

        return $this->redirectToRoute('app_basket_index');
    }

    private function findPrice(int $id)
    {
      return $this->entityManager->createQueryBuilder()
                ->select('orders.id', 'starship.name', 'starship.cost')
                ->from(Order::class, 'orders')
                ->join(Starship::class, 'starship')
                ->where('starship.id = orders.starship')
                ->andWhere('orders.id = '.$id)
                ->getQuery()->getResult();
    }

}