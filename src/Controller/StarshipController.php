<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Starship;
use App\Entity\StarshipComment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class StarshipController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/starship")
     */
    public function index(Request $request): Response
    {
        if (!$this->getUser()) {
            throw new AccessDeniedException();
        }

        $query = $request->get('q');

        $starships =
            $this->entityManager->createQueryBuilder()
            ->select('q')
            ->from(Starship::class, 'q')
            ->where('q.name LIKE \'%'.$query.'%\'')
            ->getQuery()->getResult();

        return $this->render('starship/index.html.twig', [
            'starships' => $starships,
        ]);
    }

    /**
     * @Route("/starship/view/{starship}")
     */
    public function viewAction(Request $request)
    {
        /** @var Starship $starship */
        $starship = $this->entityManager->getRepository(Starship::class)->find($request->get('starship'));

        if ($request->get('comment')) {
            $comment = new StarshipComment();
            $comment->setStarship($starship);
            $comment->setComment($request->get('comment'));

            $this->entityManager->persist($comment);
            $this->entityManager->flush();
        }

        return $this->render('starship/view.html.twig', [
            'starship' => $starship,
        ]);
    }

    /**
     * @Route("/starship/buy/{starship}")
     */
    public function buyAction(Request $request)
    {
        /** @var Starship $starship */
        $starship = $this->entityManager->getRepository(Starship::class)->find($request->get('starship'));

        if (!$this->getUser()) {
            throw new AccessDeniedException();
        }

        $cost = $starship->getCost();

        $this->getUser()->setCredits(
            $this->getUser()->getCredits() - $cost
        );

        $order = new Order();
        $order->setUser($this->getUser());
        $order->setStarship($starship);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_starship_index');
    }
}
