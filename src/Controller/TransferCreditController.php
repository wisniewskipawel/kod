<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\TransferCreditForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TransferCreditController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/transfer-credit", name="app_transfer_credit_index")
     */
    public function index(Request $request): Response
    {
        if(!$this->getUser()) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(TransferCreditForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            /** @var int $credits */
            $credits = $form->get('credits')->getData();

            if($this->getUser()->getCredits() < $credits) {
                throw new \Exception('No required credits!');
            }

            /** @var User $user */
            $user = $form->get('user')->getData();

            $user->setCredits(
                $user->getCredits() + $credits
            );

            $this->getUser()->setCredits(
                $this->getUser()->getCredits() - $credits
            );

            $this->entityManager->flush();
        }

        return $this->render('transfer-credit/index.html.twig', [
            'form' => $form->createView()
        ]);
    }



}
