<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Starship;
use App\Entity\User;
use App\Form\TransferCreditForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class OrderController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/order")
     */
    public function index(Request $request): Response
    {
        if(!$this->getUser()) {
            throw new AccessDeniedException();
        }

        if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $orders = $this->entityManager->getRepository('App:Order')->findAll();
        } else {
            $orders = $this->getUser()->getOrders();
        }

        return $this->render('order/index.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/order/delete/{order}")
     */
    public function buyAction(Request $request)
    {
        /** @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->find($request->get('order'));


        $this->getUser()->setCredits(
            $this->getUser()->getCredits() + ($order->getStarship()->getCost() / 2)
        );

        $this->entityManager->remove($order);
        $this->entityManager->flush();

        return $this->redirectToRoute('default');
    }



}
